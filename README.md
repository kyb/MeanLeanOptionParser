Description
-----------

The Lean Mean C++ Option Parser handles the program arguments (argc, argv). It supports the short and long option formats 
of getopt(), getopt_long() and getopt_long_only() but has a more convenient interface. It is a freestanding, header-only 
library with no dependencies, not even libc or STL. It comes with a usage message formatter that supports column alignment 
and line wrapping, making it ideal for localized messages with different lengths. 

[The Lean Mean C++ Option Parser Web Site](http://optionparser.sourceforge.net/)


Features
-----------
* Header-only library. Just #include "optionparser.h" and you're set.
* Freestanding. There are no dependencies whatsoever, not even the C or C++ standard library.
* Usage message formatter that supports column alignment and line wrapping.
* Unlike getopt() and derivatives it doesn't force you to loop through options sequentially. Instead you can access options directly like this:
* if ( options[QUIET] ) ... //Test for presence of a switch in the argument vector:
* if ( options[FOO].last()->type() == DISABLE ) ... // --enable-foo/--disable-foo: last one used wins
* int verbosity = options[VERBOSE].count(); // -v verbose, -vv more verbose
* for (Option* opt = options[FILE]; opt; opt = opt->next()) // go through all --file=<fname> arguments


### License
MIT License
